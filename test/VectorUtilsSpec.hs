module VectorUtilsSpec
where

import Test.Hspec
import Test.QuickCheck

import qualified Data.Vector.Mutable as MV
import qualified Data.Vector as V
import Control.Monad.ST
import VectorUtils
import Data.List

spec :: Spec
spec = do
  describe "inPlaceQuicksort" $ do
    it "should sort the same way the standard sort function does" $ property $
      \xs -> sort (xs :: [Int]) ==
        (runST $ do
            vec <- V.unsafeThaw $ V.fromList xs
            inPlaceQuicksort vec
            fmap V.toList $ V.unsafeFreeze vec
        )
