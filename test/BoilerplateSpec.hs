module BoilerplateSpec where

import Test.Hspec
import Test.QuickCheck
import Boilerplate

spec :: Spec
spec = do
  describe "fibonacciNumbers" $ do
    it "starts with 1, 1, 2, 3, 5" $ do
      take 5 fibonacciNumbers `shouldBe` [1, 1, 2, 3, 5]
    it "holds fib(n) = fib (n-1) + fib (n-1) for n >= 2" . property $
      \n -> n >= 2 ==> fibonacciNumbers!!n == fibonacciNumbers!!(n-1) + fibonacciNumbers!!(n-2)
