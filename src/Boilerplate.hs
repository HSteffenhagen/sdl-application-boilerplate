module Boilerplate where

fibonacciNumbers :: [Int]
fibonacciNumbers = go 0 1 where
  go x y =
    --  written in such a weird way to
    --  force early evaluation of sums
    let xy = x + y
    in xy `seq` (y : go y xy)
