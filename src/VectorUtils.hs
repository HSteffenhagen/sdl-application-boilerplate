module VectorUtils
where

import Control.Monad(when)
import Control.Monad.Primitive
import qualified Data.Vector.Mutable as MV

inPlaceQuicksort :: (PrimMonad m, Ord a) => MV.MVector (PrimState m) a -> m ()
inPlaceQuicksort mVector = when (MV.length mVector >= 2) $ do
  pivotLoc <- pivotize
  when (pivotLoc > 0) $ inPlaceQuicksort (MV.slice 0 pivotLoc mVector)
  when (pivotLoc + 1 < vlen) $ inPlaceQuicksort (MV.slice (pivotLoc + 1) (vlen - pivotLoc - 1) mVector)
  where
    vlen = MV.length mVector
    pivotize = pivotize' 1 0
    pivotize' i pivotLoc
      | i == vlen = return pivotLoc
      | i == pivotLoc = pivotize' pivotLoc (i+1)
      | otherwise = do
          pivot <- MV.unsafeRead mVector pivotLoc
          current <- MV.unsafeRead mVector i
          if current < pivot then do
            swap i (pivotLoc + 1)
            swap pivotLoc (pivotLoc+1)
            pivotize' (i+1) (pivotLoc+1)
          else
            pivotize' (i+1) pivotLoc
    swap i j = do
      one <- MV.unsafeRead mVector i
      other <- MV.unsafeRead mVector j
      MV.unsafeWrite mVector i other
      MV.unsafeWrite mVector j one
