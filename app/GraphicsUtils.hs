module GraphicsUtils where

import qualified Codec.Picture as Juicy
import qualified SDL
import qualified Data.Vector.Storable as SV


readTexture :: FilePath -> SDL.Renderer -> IO SDL.Texture
readTexture path renderer = do
  -- Just assume texture loading always succeeds and
  --  will always result in an rgba8 image
  -- What could go wrong?
  Right (Juicy.ImageRGBA8 image) <- Juicy.readImage path
  let imageData = Juicy.imageData image
      surfaceSize@(SDL.V2 surfaceWidth surfaceHeight)= SDL.V2
        (fromIntegral $ Juicy.imageWidth image)
        (fromIntegral $ Juicy.imageHeight image)
  thawedImageData <- SV.unsafeThaw imageData
  surface <- SDL.createRGBSurfaceFrom
    thawedImageData
    surfaceSize
    (4 * surfaceWidth)
    -- ABGR because endianness I think?
    -- might need some logic to
    -- swap these depending on platform
    SDL.ABGR8888
  texture <- SDL.createTextureFromSurface renderer surface
  SDL.freeSurface surface
  return texture
