-- this lets string literals be used for other types such as Text
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}
module Main where

import Boilerplate
import Control.Applicative
import Control.Exception(bracket)
import Control.Monad(when)
import Control.Monad.State
import Data.Foldable(traverse_)

import qualified Data.Text as T
import qualified SDL
import SDL(($=))
import Text.Printf
import GraphicsUtils(readTexture)
import Linear.Vector
import Linear.Affine
import Linear.V2

import qualified Data.Vector as V

--  CInt means “the int type from C”
import Foreign.C.Types(CInt)

screenWidth :: CInt
screenWidth = 800

screenHeight :: CInt
screenHeight = 600

assertIO :: Bool -> String -> IO ()
assertIO cond msg = do
  when (not cond) $ error ("Assertion failed: " ++ msg)

data Sprite = Sprite
  { spriteTexture :: SDL.Texture
  , spriteRectangle :: SDL.Rectangle CInt
  }

data GlobalCoordinateTransform = GlobalCoordinateTransform
  { gctScale :: Double
  , gctMargin :: V2 CInt
  } deriving Show

toInt :: Integral a => a -> Int
toInt = fromIntegral

globalCoordinateTransformFromWindow :: CInt -> CInt -> SDL.Window -> IO GlobalCoordinateTransform
globalCoordinateTransformFromWindow virtualWidth virtualHeight window = do
  printf "Virtual Dimensions: %dX%d\n" (toInt virtualWidth) (toInt virtualHeight)
  assertIO (virtualWidth > virtualHeight)
    "I'm not sure if the logic still works out otherwise"
  (SDL.V2 windowWidth windowHeight) <- SDL.get (SDL.windowSize window)
  printf "Window Dimensions: %dX%d\n" (toInt windowWidth) (toInt windowHeight)
  let virtualAspectRatio = (fromIntegral virtualWidth / fromIntegral virtualHeight) :: Double
  if fromIntegral windowWidth >= virtualAspectRatio * fromIntegral windowHeight
  then do
    let totalXMargin = floor $ fromIntegral windowWidth - virtualAspectRatio * fromIntegral windowHeight
    return $ GlobalCoordinateTransform
      { gctScale = fromIntegral windowHeight / fromIntegral virtualHeight
      , gctMargin = V2 (totalXMargin `div` 2) 0
      }
  else do
    let totalYMargin = floor $ fromIntegral windowHeight - (1/virtualAspectRatio) * fromIntegral windowWidth
    return $ GlobalCoordinateTransform
      { gctScale = fromIntegral windowWidth / fromIntegral virtualWidth
      , gctMargin = V2 0 (totalYMargin `div` 2)
      }

data SpriteSheet = SpriteSheet
  { spriteSheetSprites :: V.Vector Sprite
  , spriteSheetRows :: Int
  , spriteSheetColumns :: Int
  }

getSprite :: Int -> Int -> SpriteSheet -> Sprite
getSprite column row SpriteSheet{..} =
  spriteSheetSprites V.! (row * spriteSheetColumns + column)

spriteSheet :: FilePath -> SDL.V2 CInt -> SDL.V2 CInt -> SDL.V2 CInt -> SDL.Renderer -> IO SpriteSheet
spriteSheet path (SDL.V2 offX offY) (SDL.V2 marginX marginY) dim@(SDL.V2 dimX dimY) renderer = do
  sheetTexture <- readTexture path renderer
  (textureWidth, textureHeight) <- do
    info <- SDL.queryTexture sheetTexture
    return (SDL.textureWidth info, SDL.textureHeight info)
  let sprites = V.fromList $
       (\y x -> Sprite sheetTexture $ SDL.Rectangle (SDL.P $ SDL.V2 x y) dim)
       <$> (takeWhile (< (textureHeight - dimY)) $ iterate (+ (marginY + dimY)) offY)
       <*> (takeWhile (< (textureWidth - dimX)) $ iterate (+ (marginX + dimX)) offX)
      rows = fromIntegral $ (textureHeight - offY) `div` (marginY + dimY)
      columns = fromIntegral $ (textureWidth - offX) `div` (marginX + dimX)
  printf "Rows = %d, Columns = %d\n" rows columns
  return $ SpriteSheet
    { spriteSheetSprites = sprites
    , spriteSheetRows = rows
    , spriteSheetColumns = columns
    }

data GameState = GameState
  { gameHasQuit :: Bool
  , gameGlobalCoordinateTransform :: GlobalCoordinateTransform
  , gameWaifu :: Sprite
  , gameRotationAngle :: Double
  , gameRenderer :: SDL.Renderer
  }


initialGameState :: SDL.Window -> SDL.Renderer -> IO GameState
initialGameState window renderer = do
  gct <- globalCoordinateTransformFromWindow screenWidth screenHeight window
  waifus <- spriteSheet
    "assets/roguelikeChar_transparent.png"
    (SDL.V2 0 1)
    (SDL.V2 1 1)
    (SDL.V2 16 16)
    renderer
  return $ GameState
    { gameHasQuit = False
    , gameGlobalCoordinateTransform = gct
    , gameWaifu = getSprite 0 5 waifus
    , gameRotationAngle = 0
    , gameRenderer = renderer
    }

type GameM = StateT GameState IO

quitGame :: GameM ()
quitGame = modify (\g -> g {gameHasQuit = True})

transformPoint :: GlobalCoordinateTransform -> Point V2 CInt -> Point V2 CInt
transformPoint gct (P origin) = P $
  fmap floor (fmap fromIntegral origin ^* (gctScale gct) ^+^ (fmap fromIntegral $ gctMargin gct))

transformRect :: GlobalCoordinateTransform -> SDL.Rectangle CInt -> SDL.Rectangle CInt
transformRect gct (SDL.Rectangle origin dims) = SDL.Rectangle
  (transformPoint gct origin)
  (fmap floor (fmap fromIntegral dims ^* gctScale gct))

drawSprite :: Point V2 CInt -> Sprite -> GameM ()
drawSprite pos sprite = do
  renderer <- gets gameRenderer
  gct <- gets gameGlobalCoordinateTransform
  let transformedRectangle@(SDL.Rectangle _ dims') = transformRect gct (spriteRectangle sprite)
      targetRectangle = SDL.Rectangle (transformPoint gct pos) (fmap floor $ fmap fromIntegral dims' ^* 4.0)
  SDL.copy renderer (spriteTexture sprite) (Just $ spriteRectangle sprite) (Just targetRectangle)

drawWaifu :: GameM ()
drawWaifu = do
  waifu <- gets gameWaifu
  drawSprite (P $ V2 400 300) waifu

hasQuitGame :: GameM Bool
hasQuitGame = gets gameHasQuit

handleEvents :: GameM ()
handleEvents = SDL.pollEvents >>= traverse_ handleEvent where
  handleEvent event = case SDL.eventPayload event of
    SDL.QuitEvent -> quitGame
    _             -> return ()

-- | Contains the actual main loop. Can do all sorts of nasty things in here.
--   The renderer should be used for most 2D drawing, but we might need the
--   window for other reasons like changing the window title/icon and what not
runGame :: SDL.Window -> SDL.Renderer -> IO ()
runGame window renderer = initialGameState window renderer >>= evalStateT mainLoop where
  mainLoop = do
    handleEvents
    hasQuit <- hasQuitGame
    when (not hasQuit) $ do
      let beige = SDL.V4 200 175 125 255
          white = SDL.V4 255 255 255 255
      SDL.rendererDrawColor renderer $= white
      SDL.clear renderer
      drawWaifu
      SDL.present renderer
      mainLoop

-- | Create a window, pass it to a handler action.
--   Makes sure that the window is closed at the
--   end of the handler even if it throws an exception
withWindow :: T.Text -> SDL.WindowConfig -> (SDL.Window -> IO ()) -> IO ()
withWindow title config = bracket
  (SDL.createWindow title config)
  SDL.destroyWindow

-- | Create a renderer, pass it to the handler action.
--   Again, makes sure that the renderer is closed when
--   the handler is exited.
withRenderer :: SDL.Window -> CInt -> SDL.RendererConfig -> (SDL.Renderer -> IO ()) -> IO ()
withRenderer window index config = bracket
  (SDL.createRenderer window index config)
  SDL.destroyRenderer

main :: IO ()
main = do
  print $ take 10 fibonacciNumbers
  SDL.initialize [SDL.InitVideo]
  --  TODO replace SDL.defaultWindow with customised
  --  window config (width/height etc) if needed
  let windowConfig = SDL.defaultWindow
        { SDL.windowInitialSize = SDL.V2 1500 1000
        }
  withWindow "Boilerplate Window Title" windowConfig $ \window -> do
    withRenderer window (-1) SDL.defaultRenderer $ \renderer -> do
      runGame window renderer
