# sdl-application-boilerplate

Note that this requires SDL2 to be visible pkg-config (and of course, pkg-config).

On windows stack comes with MSYS2 so you can just do

```
stack exec pacman -- -Ss sdl2
```

and there should be multiple sdl2 packages popping up – IIRC you need the
x86-64 version but I can’t quite remember what the whole name was atm.

If you’re feeling more adventurous you can just grab the binaries or source
from https://www.libsdl.org/download-2.0.php and install it yourself


# Building

Assuming all dependencies are present, you can just type

```
stack build --fast
```

to build, and

```
stack exec sdl-application-boilerplate-exe
```

to run, or otherwise

```
stack run
```

for a shortcut that does both.

```
stack test
```

to run the tests.

Note that initial build time will take a couple of minutes as Haskell dependencies are source packages,
and it’ll need to build a couple dozen things from source to make this work. Future builds will be significantly
faster.

I haven’t set up CI for this yet but I’m pretty sure I can just tell gitlab to just cache `.stack-work` and it should
be fine.
